.PHONY: test lint build tag push deploy up down restart obliterate reload pull logs cloc help
.DEFAULT_GOAL := help

OS := $(shell uname)
DOCKER_REGISTRY :=
BUILD_NAME := pyapp
BUILD_TAG := $(shell git describe --always --tags --abbrev=0)

##@ Build tasks
test:						## Test code
	docker rm $(docker ps -qa --no-trunc --filter "status=exited") || true

lint:						## Run linters
	docker run -v ${PWD}/Dockerfile:/Dockerfile:ro redcoolbeans/dockerlint -p

build:						## Build docker container
	docker build . -t ${BUILD_NAME}

tag:						## Tag containers
	docker tag ${BUILD_NAME}:latest ${DOCKER_REGISTRY}${BUILD_NAME}:${BUILD_TAG}
	docker tag ${BUILD_NAME}:latest ${DOCKER_REGISTRY}${BUILD_NAME}:latest

push:						## Push containers to registry
	docker login -u ${DOCKER_REPO_USER} -p ${DOCKER_REPO_TOKEN} https://${DOCKER_REGISTRY}
	docker push ${DOCKER_REGISTRY}${BUILD_NAME}:${BUILD_TAG}
	docker push ${DOCKER_REGISTRY}${BUILD_NAME}:latest

deploy:						## Deploy
	docker-compose up -d

##@ Service managementcontainers
up:
	docker-compose up -d

down:						## Bring down containers
	docker-compose down

restart: down up			## Restart containers

obliterate:					## Destroy containersand volumes, use with care!
	docker-compose down -v

reload:						## Restart docker containers
	docker-compose restart

pull:						## Pull docker containers
	docker-compose pull

logs:						## Show docker logs
	docker-compose logs -f

##@ Helpers
cloc:						## Show Lines of Code analysis
	@cloc --vcs git --quiet

help:						## Display this help
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n"} /^[a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)
