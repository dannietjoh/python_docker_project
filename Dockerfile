FROM python:latest
RUN adduser pyuser
COPY src /src
USER pyuser
ENTRYPOINT ["python", "/src/main.py"]
